function line_check(a, b, p)
{
  return b.sub(a).cross(p.sub(a));
}

function level_data()
{
  function con()
  {
    this.ball_spawn = vec2(0, 0);
    this.red_spawn = vec2(-128, 0);
    this.blue_spawn = vec2(128, 0);
    this.red_goal = vec2(-256, 0);
    this.blue_goal = vec2(256, 0);
    this.sling_points = array();
    this.ball_barriers = array();
    this.shapes = array();
    this.vertex = function(i, j)
    {
      if (i < 0 || i >= this.shapes.length) {
        return null;
      }
      return this.shapes[i][
        (j + this.shapes[i].length) % this.shapes[i].length];
    }
  }
  return new con();
}

function get_shape_orientation(shape)
{
  var total = 0;
  for (var i = 0; i < shape.length; ++i) {
    var a = shape[i];
    var b = shape[(1 + i) % shape.length];
    total += (b.x - a.x) * (b.y + a.y);
  }
  return total >= 0;
}

function orient_shapes(level)
{
  for (var i = 0; i < level.shapes.length; ++i) {
    if (!get_shape_orientation(level.shapes[i])) {
      level.shapes[i].reverse();
    }
  }
}

function merge_level(level)
{
  var t = vec2(0, 0);
  var new_level = load_obj(save_obj(level), t);
  return new_level.shapes.length ? new_level : null;
}

function parse_obj_raw(text, vertices, faces, scale)
{
  var level = level_data();
  scale.x = 1;
  scale.y = 1;

  var lines = text.split('\n');
  for (var i = 0; i < lines.length; ++i) {
    var words = lines[i].split(/[ \t]+/).filter(function(x) {return x != ''});
    if (!words.length) {
      continue;
    }

    // Split out the # if it's there.
    if (words[0].length > 1 && words[0].charAt(0) == '#') {
      words[0] = words[0].substr(1);
      words.unshift('#');
    }

    // Handle # directives.
    if (words[0] == '#' && words.length > 1) {
      var target = vec2(0, 0);
      var target2 = vec2(0, 0);
      var is_scale = false;
      switch (words[1].toLowerCase()) {
        case '.scale':
          target = scale;
          is_scale = true;
          break;
        case '.ballspawn':
          target = level.ball_spawn;
          break;
        case '.redspawn':
          target = level.red_spawn;
          break;
        case '.bluespawn':
          target = level.blue_spawn;
          break;
        case '.redgoal':
          target = level.red_goal;
          break;
        case '.bluegoal':
          target = level.blue_goal;
          break;
      }
      var assign = function(v, x, i) {
        if (words.length > i) {
          if (x) {
            v.x = parseFloat(words[i]) * (is_scale ? 1 : scale.x);
          }
          else {
            v.y = parseFloat(words[i]) * (is_scale ? 1 : scale.y);
          }
        }
      };
      assign(target, true, 2);
      assign(target, false, 3);
      assign(target2, true, 4);
      assign(target2, false, 5);
      switch (words[1].toLowerCase()) {
        case '.slingpoint':
          level.sling_points.push(target);
          break;
        case '.ballbarrier':
          level.ball_barriers.push([target, target2]);
          break;
      }
    }

    // Handle vertices.
    if (words[0] == 'v' && words.length >= 3) {
      var v = vec2(parseFloat(words[1]), parseFloat(words[2]));
      v.x *= scale.x;
      v.y *= scale.y;
      vertices.push(v);
    }

    // Handle faces.
    if (words[0] == 'f' && words.length >= 4) {
      var a = parseInt(words[1]);
      var b = parseInt(words[2]);
      var c = parseInt(words[3]);
      faces.push(face(a - 1, b - 1, c - 1));
    }
  }

  return level;
}

function load_obj(text, scale)
{
  var vertices = array();
  var faces = array();
  var level = parse_obj_raw(text, vertices, faces, scale);

  // Load faces into arrays.
  for (var i = 0; i < faces.length; ++i) {
    var s = array();
    s.push(faces[i].a);
    s.push(faces[i].b);
    s.push(faces[i].c);
    level.shapes.push(s);
  }

  function merge(a, b)
  {
    var adjacent = false;
    var i;
    var j;
    var ii;
    var jj;

    for (i = 0; i < level.shapes[a].length; ++i) {
      ii = (1 + i) % level.shapes[a].length;
      for (j = 0; j < level.shapes[b].length; ++j) {
        jj = (1 + j) % level.shapes[b].length;

        if (level.shapes[a][i] == level.shapes[b][jj] &&
            level.shapes[a][ii] == level.shapes[b][j]) {
          adjacent = true;
          break;
        }
      }
      if (adjacent) {
        break;
      }
    }

    if (!adjacent) {
      return false;
    }

    var b_slice;
    if (jj == 0) {
      b_slice = level.shapes[b].slice(1, 1 + level.shapes[b].length - 2);
    }
    else {
      b_slice =
          level.shapes[b].slice(1 + jj).concat(level.shapes[b].slice(0, j));
    }
    level.shapes[a] = level.shapes[a].slice(0, ii).concat(b_slice).concat(
        level.shapes[a].slice(ii));
    level.shapes.splice(b, 1);
    return true;
  }

  // Merge with self.
  function merge_self()
  {
    for (var i = 0; i < level.shapes.length; ++i) {
      for (var j = 0; j < level.shapes[i].length; ++j) {
        var jj = (1 + j) % level.shapes[i].length;
        if (level.shapes[i][j] == level.shapes[i][jj]) {
          level.shapes[i].splice(j, 1);
          --j;
          continue;
        }
        var jj = (2 + j) % level.shapes[i].length;
        if (level.shapes[i][j] == level.shapes[i][jj]) {
          level.shapes[i].splice(j, 2);
          --j;
        }
      }
      if (level.shapes[i].length < 3) {
        level.shapes.splice(i, 1);
        --i;
      }
    }
  }

  merge_self();
  // Merge faces.
  for (var i = 0; i < level.shapes.length; ++i) {
    for (var j = i + 1; j < level.shapes.length; ++j) {
      if (merge(i, j)) {
        i = 0;
        j = i;
      }
    }
  }
  merge_self();

  // Remove irrelevant vertices.
  var vertex_count = array();
  for (var i = 0; i < level.shapes.length; ++i) {
    for (var j = 0; j < level.shapes[i].length; ++j) {
      while (vertex_count.length <= level.shapes[i][j]) {
        vertex_count.push(0);
      }
      ++vertex_count[level.shapes[i][j]];
    }
  }
  for (var i = 0; i < level.shapes.length; ++i) {
    for (var j = 0; j < level.shapes[i].length; ++j) {
      if (vertex_count[level.shapes[i][j]] > 1) {
        continue;
      }
      var a = vertices[level.vertex(i, j - 1)];
      var b = vertices[level.vertex(i, j)];
      var c = vertices[level.vertex(i, 1 + j)];
      if (Math.abs(line_check(b, a, c)) < 1 / 128) {
        level.shapes[i].splice(j, 1);
        --j;
      }
    }
  }

  // Map indices to vertices.
  for (var i = 0; i < level.shapes.length; ++i) {
    for (var j = 0; j < level.shapes[i].length; ++j) {
      level.shapes[i][j] = vertices[level.shapes[i][j]].copy();
    }
  }
  return level;
}

function write_obj_raw(level, vertices, faces)
{
  var text = '';
  function write_vec2(name, v)
  {
    text += '# .' + name + ' ' + v.x + ' ' + v.y + '\n';
  }
  function write_vec2x2(name, v, w)
  {
    text += '# .' + name + ' ' + v.x + ' ' + v.y + ' ' + w.x + ' ' + w.y + '\n';
  }
  write_vec2('scale', vec2(1, 1));
  write_vec2('ballspawn', level.ball_spawn);
  write_vec2('redspawn', level.red_spawn);
  write_vec2('redgoal', level.red_goal);
  write_vec2('bluespawn', level.blue_spawn);
  write_vec2('bluegoal', level.blue_goal);
  for (var i = 0; i < level.sling_points.length; ++i) {
    write_vec2('slingpoint', level.sling_points[i]);
  }
  for (var i = 0; i < level.ball_barriers.length; ++i) {
    write_vec2x2('ballbarrier',
                 level.ball_barriers[i][0], level.ball_barriers[i][1]);
  }
  text += '# http://seiken.co.uk/slingedit\n';
  for (var i = 0; i < vertices.length; ++i) {
    text += 'v ' + vertices[i].x.toFixed(5) + ' ' +
                   vertices[i].y.toFixed(5) + ' 0.0\n';
  }
  text += 's off\n';
  for (var i = 0; i < faces.length; ++i) {
    text += 'f ' +
        (1 + faces[i].a) + ' ' +
        (1 + faces[i].b) + ' ' +
        (1 + faces[i].c) + '\n';
  }
  return text;
}

function triangulate(level)
{
  orient_shapes(level);

  // Ear-clipping triangulation.
  var faces = array();
  for (var i = 0; i < level.shapes.length; ++i) {
    var copy = level.shapes[i].slice(0);

    while (copy.length >= 3) {
      // Merge zero-area triangles. This doesn't really need to look at every
      // vertex every iteration, but eh. In fact, this may be unnecessary with
      // the fix to ear-finding below.
      for (var j = 0; j < copy.length; ++j) {
        var a = (j + copy.length - 1) % copy.length;
        var b = j;
        var c = (1 + j) % copy.length;

        if (line_check(copy[a], copy[b], copy[c]) == 0) {
          // Check the vertices are out-of-order.
          var ba = copy[a].sub(copy[b]);
          var bc = copy[c].sub(copy[b]);
          if (line_check(vec2(0, 0), ba.normal(), ba) > 0 ==
              line_check(vec2(0, 0), ba.normal(), bc) > 0) {
            copy.splice(b, 1);
            j = Math.max(j - 1, 0);
          }
        }
      }

      var found_ear = false;
      for (var j = 0; j < copy.length; ++j) {
        var a = (j + copy.length - 1) % copy.length;
        var b = j;
        var c = (1 + j) % copy.length;

        if (line_check(copy[a], copy[b], copy[c]) > 0) {
          continue;
        }

        var is_ear = true;
        for (var k = (1 + c) % copy.length; k != a; k = (1 + k) % copy.length) {
          var ab = line_check(copy[a], copy[b], copy[k]);
          var bc = line_check(copy[b], copy[c], copy[k]);
          var ca = line_check(copy[c], copy[a], copy[k]);
          // Problem: comparisons below needs to be strict so that we don't
          // care about points on 'the other side', like in a donut where the
          // edges match up. But needs to be non-strict so that we don't take
          // an ear that leaves zero-area parts.
          // Solution: if any are zero, only care when the points are going the
          // wrong way.
          if (ab <= 0 && bc <= 0 && ca <= 0) {
            var edges = (ab == 0 ? 1 : 0) +
                (bc == 0 ? 1 : 0) + (ca == 0 ? 1 : 0);
            if (edges >= 1) {
              // Point lies on edge. We care only if it's turning left and it's
              // not a corner.
              var kprev = copy[(k + copy.length - 1) % copy.length];
              var knext = copy[(1 + k) % copy.length];
              if (line_check(kprev, copy[k], knext) > 0 && edges <= 1) {
                is_ear = false;
              }
            }
            else {
              is_ear = false; 
            }
          }
        }

        if (!is_ear) {
          continue;
        }

        faces.push(face(copy[a].copy(),
                        copy[b].copy(),
                        copy[c].copy()));
        copy.splice(b, 1);
        found_ear = true;
        break;
      }
      if (!found_ear) {
        return 'invalid geometry';
      }
    }
  }
  return faces;
}

function triangulate_to_level(level)
{
  var new_level = level_data();
  new_level.ball_spawn = level.ball_spawn.copy();
  new_level.red_spawn = level.red_spawn.copy();
  new_level.blue_spawn = level.blue_spawn.copy();
  new_level.red_goal = level.red_goal.copy();
  new_level.blue_goal = level.blue_goal.copy();
  for (var i = 0; i < level.sling_points.length; ++i) {
    new_level.sling_points.push(level.sling_points[i].copy());
  }
  var faces = triangulate(level);
  for (var i = 0; i < faces.length; ++i) {
    new_level.shapes.push([faces[i].a, faces[i].b, faces[i].c]);
  }
  return new_level;
}

function save_obj(level)
{
  var faces = triangulate(level);
  if (typeof faces == 'string' || faces instanceof String) {
    return faces;
  }

  // Map the vertices to indices.
  var map = {};
  var vertices = array();
  var count = 0;
  function handle(v)
  {
    var key = v.x + ' ' + v.y;
    if (key in map) {
      return;
    }
    map[key] = count++;
    vertices.push(v);
  }
  for (var i = 0; i < faces.length; ++i) {
    handle(faces[i].a);
    handle(faces[i].b);
    handle(faces[i].c);
  }
  for (var i = 0; i < faces.length; ++i) {
    var f = faces[i];
    faces[i] = face(map[f.a.x + ' ' + f.a.y],
                    map[f.b.x + ' ' + f.b.y],
                    map[f.c.x + ' ' + f.c.y]);
  }

  return write_obj_raw(level, vertices, faces);
}
