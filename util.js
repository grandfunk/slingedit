function show(element, bool)
{
  if (bool) {
    element.style.zIndex = 10;
    element.style.visibility = 'visible';
  }
  else {
    element.style.zIndex = -10;
    element.style.visibility = 'hidden';
  }
}

function shown(element)
{
  return element.style.visibility == 'visible';
}

function array()
{
  return new Array();
}

vec2_copy = function(v)
{
  return vec2(this.x, this.y);
}
vec2_add = function(v)
{
  return vec2(this.x + v.x, this.y + v.y);
}
vec2_sub = function(v)
{
  return vec2(this.x - v.x, this.y - v.y);
}
vec2_mul = function(s)
{
  return vec2(s * this.x, s * this.y);
}
vec2_div = function(s)
{
  return vec2(this.x / s, this.y / s);
}
vec2_neg = function()
{
  return vec2(-this.x, -this.y);
}
vec2_normal = function()
{
  return vec2(this.y, -this.x);
}
vec2_dot = function(v)
{
  return this.x * v.x + this.y * v.y;
}
vec2_cross = function(v)
{
  return this.x * v.y - this.y * v.x;
}
vec2_lerp = function(v, t)
{
  return this.add(v.sub(this).mul(t));
}
vec2_lensq = function()
{
  return this.x * this.x + this.y * this.y;
}
vec2_len = function()
{
  return Math.sqrt(this.lensq());
}
vec2_normalise = function()
{
  return this.div(this.len());
}
vec2_angle = function()
{
  return Math.atan2(this.y, this.x);
}
vec2_eq = function(v)
{
  return Math.abs(this.x - v.x) < 1 / 1024 &&
         Math.abs(this.y - v.y) < 1 / 1024; 
}

function vec2_con(x, y)
{
  this.x = x;
  this.y = y;
  this.copy = vec2_copy;
  this.add = vec2_add;
  this.sub = vec2_sub;
  this.mul = vec2_mul;
  this.div = vec2_div;
  this.neg = vec2_neg;
  this.normal = vec2_normal;
  this.dot = vec2_dot;
  this.cross = vec2_cross;
  this.lerp = vec2_lerp;
  this.lensq = vec2_lensq;
  this.len = vec2_len;
  this.normalise = vec2_normalise;
  this.angle = vec2_angle;
  this.eq = vec2_eq;
}

function vec2(x, y)
{
  return new vec2_con(x, y);
}

function face(a, b, c)
{
  function con(a, b, c)
  {
    this.a = a;
    this.b = b;
    this.c = c;
  }
  return new con(a, b, c);
}

function div(a, b)
{
  return Math.floor(Math.floor(a) / Math.floor(b));
}

function mod(a, b)
{
  return (b + a % b) % b;
}

function undo_stack()
{
  function con()
  {
    this.undo_stack = array();
    this.redo_stack = array();
    this.add = function(x)
    {
      x.redo();
      this.add_no_do(x);
    }
    this.add_no_do = function(x)
    {
      this.undo_stack.push(x);
      this.redo_stack = array();
    }
    this.redo = function()
    {
      if (this.redo_stack.length) {
        var x = this.redo_stack[this.redo_stack.length - 1];
        x.redo();
        this.undo_stack.push(x);
        this.redo_stack.splice(this.redo_stack.length - 1, 1);
      }
    }
    this.undo = function()
    {
      if (this.undo_stack.length) {
        var x = this.undo_stack[this.undo_stack.length - 1];
        x.undo();
        this.redo_stack.push(x);
        this.undo_stack.splice(this.undo_stack.length - 1, 1);
      }
    }
  }
  return new con();
}

function compound_action(list)
{
  this.list = list;
  this.redo = function()
  {
    for (var i = 0; i < this.list.length; ++i) {
      this.list[i].redo();
    }
  }
  this.undo = function()
  {
    for (var i = this.list.length - 1; i >= 0; --i) {
      this.list[i].undo();
    }
  }
}

function move_action(v, to)
{
  this.old = null;
  this.v = v;
  this.to = to;
  this.redo = function()
  {
    this.old = v.copy();
    this.v.x = this.to.x;
    this.v.y = this.to.y;
  }
  this.undo = function()
  {
    this.v.x = this.old.x;
    this.v.y = this.old.y;
  }
}

function add_action(a, i, e)
{
  this.a = a;
  this.i = i;
  this.e = e;
  this.undo = function()
  {
    this.a.splice(i, 1);
  }
  this.redo = function()
  {
    if (this.i < this.a.length) {
      var len = this.a.length;
      this.a.push(this.a[len - 1]);
      for (var j = len - 1; j > this.i; --j) {
        this.a[j] = this.a[j - 1];
      }
      this.a[this.i] = this.e;
    }
    else {
      this.a.push(this.e);
    }
  }
}

function splice_action(a, i, b)
{
  this.a = a;
  this.i = i;
  this.b = b;
  this.e = null;
  this.redo = function()
  {
    this.e = this.a[this.i];
    var args = [this.i, 1].concat(this.b);
    Array.prototype.splice.apply(this.a, args);
  }
  this.undo = function()
  {
    this.a.splice(this.i, this.b.length, this.e);
  }
}

function delete_action(a, i)
{
  this.a = a;
  this.i = i;
  this.e = null;
  this.redo = function()
  {
    this.e = this.a[this.i];
    this.a.splice(i, 1);
  }
  this.undo = function()
  {
    if (this.i < this.a.length) {
      var len = this.a.length;
      this.a.push(this.a[len - 1]);
      for (var j = len - 1; j > this.i; --j) {
        this.a[j] = this.a[j - 1];
      }
      this.a[this.i] = this.e;
    }
    else {
      this.a.push(this.e);
    }
  }
}

function angle_sign(a, b, c)
{
  return a < b != c < b ? a < b : a > c;
}

function angle_dist(a, b)
{
  return a < b ? b - a : 2 * Math.PI + b - a;
}

function line_intersect(a, av, b, bv)
{
  var t = av.cross(b.sub(a)) / bv.cross(av);
  return b.add(bv.mul(t));
}

function get_circular_curve(prev, mid, next, grid_size)
{
  var mp_normal = prev.sub(mid).normal();
  var mn_normal = next.sub(mid).normal();

  var mp_mid = mid.add(prev).div(2);
  var mn_mid = mid.add(next).div(2);

  var origin = line_intersect(mp_mid, mp_normal, mn_mid, mn_normal);
  var radius = mid.sub(origin).len();

  var prev_angle = prev.sub(origin).angle();
  var mid_angle = mid.sub(origin).angle();
  var next_angle = next.sub(origin).angle();

  var pos = angle_sign(prev_angle, mid_angle, next_angle);
  var dist = pos ? angle_dist(prev_angle, next_angle) :
                   angle_dist(next_angle, prev_angle);
  var points = Math.floor((radius / grid_size) * (dist / 2));
  points = Math.max(1, points);

  var a = array();
  for (var i = 1; i < points; ++i) {
    var angle = prev_angle + (pos ? 1 : -1) * dist * (i / points);
    var v = vec2(Math.cos(angle), Math.sin(angle));
    a.push(origin.add(v.mul(radius)));
  }
  return a;
}

function get_elliptical_curve(prev, mid, next, grid_size)
{
  var origin = prev.add(next).div(2);
  var rmid = origin.mul(2).sub(mid);

  var cnext = mid.add(next).sub(origin);
  var cprev = mid.add(prev).sub(origin);

  var rcnext = rmid.add(next).sub(origin);
  var rcprev = rmid.add(prev).sub(origin);

  var points = Math.floor(
      Math.max(prev.sub(origin).len(), mid.sub(origin).len()) * Math.PI /
      (4 * grid_size));
  points = Math.max(1, points);

  var a = array();
  for (var i = 1; i < points; ++i) {
    var t = i / points;
    var x = prev.lerp(cprev, t);
    var y = prev.lerp(origin, t);
    a.push(line_intersect(mid, x.sub(mid), rmid, y.sub(rmid)));
  }
  for (var i = 0; i < points; ++i) {
    var t = i / points;
    var x = cnext.lerp(next, t);
    var y = origin.lerp(next, t);
    a.push(line_intersect(mid, x.sub(mid), rmid, y.sub(rmid)));
  }

  return a;
}

function get_bezier_curve(prev, mid, next, grid_size)
{
  var prev_len = prev.sub(mid).len();
  var next_len = next.sub(mid).len();
  var points = Math.floor((prev_len + next_len) / (2 * grid_size));
  points = Math.max(1, points);

  var arr = array();
  for (var i = 1; i < points; ++i) {
    var t = i / points;
    arr.push(prev.lerp(mid, t).lerp(mid.lerp(next, t), t));
  }
  return arr;
}
