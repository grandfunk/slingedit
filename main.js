// TODO: warn on bad geometry or symmetry-violating geometry.
// TODO: triangulation still makes zero-area triangles sometimes, e.g.
// on example level.

var body = document.getElementById('body');
var canvas = document.getElementById('canvas');
var load_textarea = document.getElementById('load');
var save_textarea = document.getElementById('save');
var context = canvas.getContext('2d');

var temp_scale = vec2(1, 1);
var level = load_obj(example_data.join('\n'), temp_scale);
var undo = undo_stack();
var add_shape_points = null;
var split_shape_points = null;
var add_barrier_points = null;

var mouse = new function()
{
  this.origin = vec2(0, 0);
  this.button = 0;
  this.drag_start = vec2(0, 0);
  this.drag_objects = null;
}

var camera = new function()
{
  this.origin = vec2(0, 0);
  this.size = vec2(0, 0);
  this.grid_size = temp_scale.x;
  this.zoom = 1;
  this.min = function()
  {
    return this.origin.sub(this.size.div(this.zoom * 2));
  }
  this.max = function()
  {
    return this.origin.add(this.size.div(this.zoom * 2));
  }
  this.to = function(v)
  {
    return vec2(v.x, -v.y).sub(this.min()).mul(this.zoom);
  }
  this.from = function(v)
  {
    var c = v.div(this.zoom).add(this.min());
    return vec2(c.x, -c.y);
  }
  this.from_grid = function(v)
  {
    var snap = this.from(v).add(vec2(this.grid_size, this.grid_size).div(2));
    return snap.sub(vec2(mod(snap.x, this.grid_size),
                         mod(snap.y, this.grid_size)));
  }
}

var editing = new function()
{
  function object_ref(symmetry_key, object)
  {
    this.symmetry_key = symmetry_key;
    this.object = object;
  }
  function edge_ref(symmetry_key, i, j)
  {
    this.symmetry_key = symmetry_key;
    this.i = i;
    this.j = j;
  }
  
  this.helper = 0;
  this.symmetric = false;
  this.closest_objects = array();
  this.closest_edges = array();
  this.get_symmetries = function()
  {
    var list = array();
    // Horizontal symmetry.
    if (this.helper == 1 || this.helper == 4) {
      list.push(1);
    }
    // Vertical symmetry.
    if (this.helper == 2 || this.helper == 4) {
      list.push(2);
    }
    // Pi-rotational symmetry.
    if (this.helper == 3 || this.helper == 4 || this.helper == 5) {
      list.push(3);
    }
    // Half-pi-rotational symmetry.
    if (this.helper == 5) {
      list.push(4);
      list.push(5);
    }
    list.push(0); 
    return list;
  }
  this.is_point_symmetric = function(a, b)
  {
    var symmetries = this.get_symmetries();
    for (var i = 0; i < symmetries.length; ++i) {
      if (this.symmetry_point(a, symmetries[i]).eq(b)) {
        return true;
      }
    }
    return false;
  }
  this.symmetry_point = function(v, symmetry)
  {
    if (symmetry == 1) {
      return vec2(-v.x, v.y);
    }
    if (symmetry == 2) {
      return vec2(v.x, -v.y);
    }
    if (symmetry == 3) {
      return vec2(-v.x, -v.y);
    }
    if (symmetry == 4) {
      return vec2(v.y, -v.x);
    }
    if (symmetry == 5) {
      return vec2(-v.y, v.x);
    }
    return v;
  }
  this.get_closest_object = function(mouse, level)
  {
    var first = true;
    var dist = 0;
    var closest = null;
    function handle(v)
    {
      var d = v.sub(mouse).lensq();
      if (d < 256 && (first || d < dist)) {
        dist = d;
        closest = new object_ref(-1, v);
        first = false;
      }
    }

    handle(level.ball_spawn);
    handle(level.red_spawn);
    handle(level.red_goal);
    handle(level.blue_spawn);
    handle(level.blue_goal);
    for (var i = 0; i < level.sling_points.length; ++i) {
      handle(level.sling_points[i]);
    }
    for (var i = 0; i < level.ball_barriers.length; ++i) {
      handle(level.ball_barriers[i][0]);
      handle(level.ball_barriers[i][1]);
    }
    for (var i = 0; i < level.shapes.length; ++i) {
      for (var j = 0; j < level.shapes[i].length; ++j) {
        handle(level.shapes[i][j]);
      }
    }
    return closest;
  }
  this.get_closest_edge = function(mouse, level)
  {
    var first = true;
    var dist = 0;
    var closest = null;
    function handle(i, j)
    {
      var a = level.vertex(i, j);
      var b = level.vertex(i, 1 + j);

      var n = b.sub(a).normalise();
      var ma = a.sub(mouse);
      var d = ma.sub(n.mul(ma.dot(n))).lensq();

      var mid = a.add(b).div(2);
      var dmid = mouse.sub(mid).lensq();
      var dline = mid.sub(b).lensq();

      if (d < 256 && dmid < dline + 256 && (first || d < dist)) {
        dist = d;
        closest = new edge_ref(-1, i, j);
        first = false;
      }
    }

    for (var i = 0; i < level.shapes.length; ++i) {
      for (var j = 0; j < level.shapes[i].length; ++j) {
        handle(i, j);
      }
    }
    return closest;
  }
  this.get_symmetric_closest_objects = function(mouse, level)
  {
    var list = array();
    var closest = this.get_closest_object(mouse, level);
    if (!closest) {
      return list;
    }
    closest.symmetry_key = 0;
    list.push(closest);
    if (!this.symmetric) {
      return list;
    }
    var symmetries = this.get_symmetries();
    for (var i = 0; i < symmetries.length - 1; ++i) {
      var c = this.get_closest_object(
          this.symmetry_point(mouse, symmetries[i]), level);
      var unused = true;
      for (var j = 0; c && j < list.length; ++j) {
        if (list[j].object == c.object) {
          unused = false;
          break;
        }
      }
      if (c && unused &&
          this.is_point_symmetric(closest.object, c.object)) {
        c.symmetry_key = symmetries[i];
        list.push(c);
      }
    }
    return list;
  }
  this.get_symmetric_closest_edges = function(mouse, level)
  {
    var list = array();
    var closest = this.get_closest_edge(mouse, level);
    if (!closest) {
      return list;
    }
    closest.symmetry_key = 0;
    list.push(closest);
    if (!this.symmetric) {
      return list;
    }
    var symmetries = this.get_symmetries();
    var a = level.vertex(closest.i, closest.j);
    var b = level.vertex(closest.i, 1 + closest.j);

    for (var i = 0; i < symmetries.length - 1; ++i) {
      var c = this.get_closest_edge(
          this.symmetry_point(mouse, symmetries[i]), level);
      var unused = true;
      for (var j = 0; c && j < list.length; ++j) {
        if (list[j].i == c.i && list[j].j == c.j) {
          unused = false;
          break;
        }
      }
      if (!c || !unused) {
        continue;
      }
      var aa = level.vertex(c.i, c.j);
      var bb = level.vertex(c.i, 1 + c.j);
      if ((this.is_point_symmetric(a, aa) &&
           this.is_point_symmetric(b, bb)) ||
          (this.is_point_symmetric(a, bb) &&
           this.is_point_symmetric(b, aa))) {
        c.symmetry_key = symmetries[i];
        list.push(c);
      }
    }
    return list;
  }
  this.update = function(mouse, level)
  {
    this.closest_objects = this.get_symmetric_closest_objects(mouse, level);
    this.closest_edges = array();
    if (!this.closest_objects || !this.closest_objects.length) {
      this.closest_edges = this.get_symmetric_closest_edges(mouse, level);
    }
  }
}

function change_level_action(camera, new_level, new_grid_size)
{
  this.camera = camera;
  this.new_grid_size = new_grid_size;
  this.old = level;
  this.new_level = new_level;
  this.redo = function()
  {
    this.old_grid_size = this.camera.grid_size;
    level = this.new_level;
    if (this.new_grid_size > 2) {
      this.camera.grid_size = this.new_grid_size;
    }
    else if (this.new_grid_size > 0) {
      this.camera.grid_size = 16;
    }
  }
  this.undo = function()
  {
    level = this.old;
    this.camera.grid_size = this.old_grid_size;
  }
}

function update()
{
  camera.size = vec2(window.innerWidth, window.innerHeight);
  canvas.width = camera.size.x;
  canvas.height = camera.size.y;

  load_textarea.style.left = camera.size.x / 2 -
      load_textarea.clientWidth / 2 + 'px';

  load_textarea.style.top = camera.size.y / 2 -
  load_textarea.clientHeight / 2 + 'px';

  save_textarea.style.left = camera.size.x / 2 -
      save_textarea.clientWidth / 2 + 'px';

  save_textarea.style.top = camera.size.y / 2 -
  save_textarea.clientHeight / 2 + 'px';

  // Handle drag screen.
  if (mouse.button == 2) {
    camera.origin = camera.origin.add(
        mouse.drag_start.sub(mouse.origin).div(camera.zoom));
    mouse.drag_start = mouse.origin;
  }
}

function draw_line(a, b, width, symmetry)
{
  var ca = camera.to(editing.symmetry_point(a, symmetry));
  var cb = camera.to(editing.symmetry_point(b, symmetry));

  context.lineWidth = width;
  context.beginPath();
  context.moveTo(ca.x, ca.y);
  context.lineTo(cb.x, cb.y);
  context.stroke();
}

function draw_object(origin, name, colour, width, radius, symmetry)
{
  context.lineWidth = width;
  context.fillStyle = colour;
  context.strokeStyle = colour;
  context.textAlign = 'center';
  context.textBaseline = 'middle';

  var co = camera.to(editing.symmetry_point(origin, symmetry));
  var size = vec2(16, 16);

  var r = co.sub(size.div(2));
  context.strokeRect(r.x, r.y, size.x, size.y);
  context.beginPath();
  if (radius) {
    context.arc(co.x, co.y, radius * camera.zoom, 0, 2 * Math.PI);
  }
  context.stroke();
  context.fillText(name, co.x, co.y);
}

function draw_text(text, origin)
{
  var lines = text.split('\n');
  for (var i = 0; i < lines.length; ++i) {
    context.fillText(lines[i], origin.x, origin.y + 12 * i);
  }
}

function fill_level(level)
{
  // Fill shapes.
  context.fillStyle = '#111';
  for (var i = 0; i < level.shapes.length; ++i) {
    context.beginPath();
    for (var j = 0; j < level.shapes[i].length; ++j) {
      var a = camera.to(level.shapes[i][j]);
      if (j == 0) {
        context.moveTo(a.x, a.y);
      }
      else {
        context.lineTo(a.x, a.y);
      }
    }
    context.closePath();
    context.fill();
  }
}

function draw_level(level, symmetry)
{
  var closest_objects = editing.closest_objects;
  var closest_edges = editing.closest_edges;

  function change_drag(v)
  {
    if (!mouse.drag_objects) {
      return v;
    }
    for (var i = 0; i < mouse.drag_objects.length; ++i) {
      if (v == mouse.drag_objects[i].object) {
        return editing.symmetry_point(camera.from_grid(mouse.origin),
                                      mouse.drag_objects[i].symmetry_key);
      }
    }
    return v;
  }

  // Draw shapes.
  context.strokeStyle = symmetry ? '#333' : '#999';
  for (var i = 0; i < level.shapes.length; ++i) {
    for (var j = 0; j < level.shapes[i].length; ++j) {
      var a = level.vertex(i, j);
      var b = level.vertex(i, 1 + j);
      var is_closest_edge = false;
      for (var k = 0; k < closest_edges.length; ++k) {
        if (closest_edges[k].i == i && closest_edges[k].j == j) {
          is_closest_edge = true;
          break;
        }
      }
      a = change_drag(a);
      b = change_drag(b);
      draw_line(a, b, is_closest_edge && !mouse.drag_objects &&
                      !add_shape_points && !split_shape_points &&
                      !add_barrier_points ? 2 : 1,
                      symmetry);
      if (!symmetry) {
        var v = camera.to(a);
        var is_closest = false;
        for (var k = 0; k < closest_objects.length; ++k) {
          if (closest_objects[k].object == level.shapes[i][j]) {
            is_closest = true;
            break;
          }
        }
        context.lineWidth =
            is_closest && !mouse.drag_objects && !add_shape_points &&
            !add_barrier_points &&
            (!split_shape_points || !split_shape_points.length ||
             !split_shape_points[0].length ||
             level.shapes[i][j] != split_shape_points[0][0].object) ? 2 : 1;
        context.strokeRect(v.x - 4, v.y - 4, 8, 8);
      }
    }
  }

  if (editing.symmetric) {
    context.strokeStyle = '#999';
  }
  if (add_shape_points && add_shape_points.length) {
    draw_line(add_shape_points[0],
              camera.from_grid(mouse.origin), 1, symmetry);
    for (var i = 0; i < add_shape_points.length - 1; ++i) {
      draw_line(add_shape_points[i], add_shape_points[1 + i], 1, symmetry);
      var v = camera.to(add_shape_points[i]);
      context.strokeRect(v.x - 4, v.y - 4, 8, 8);
    }
    draw_line(add_shape_points[add_shape_points.length - 1],
              camera.from_grid(mouse.origin), 1, symmetry);
    var v = camera.to(add_shape_points[add_shape_points.length - 1]);
    context.strokeRect(v.x - 4, v.y - 4, 8, 8);
  }
  if (add_barrier_points && add_barrier_points.length) {
    context.strokeStyle = !symmetry || editing.symmetric ? '#0c0' : '#060';
    draw_line(add_barrier_points[0],
              camera.from_grid(mouse.origin), 1, symmetry);
    var v = camera.to(add_barrier_points[0]);
    context.strokeRect(v.x - 4, v.y - 4, 8, 8);
  }
  if (split_shape_points && split_shape_points.length && !symmetry) {
    for (var i = 0; i < split_shape_points[0].length; ++i) {
      var drawn = false;
      for (var j = 0; closest_objects && j < closest_objects.length; ++j) {
        if (split_shape_points[0][i].symmetry_key !=
            closest_objects[j].symmetry_key) {
          continue;
        }
        drawn = true;
        context.strokeStyle = '#999';
        draw_line(split_shape_points[0][i].object, closest_objects[j].object,
                  1, 0);
      }
      if (!drawn) {
        context.strokeStyle = '#333';
        draw_line(split_shape_points[0][i].object,
                  editing.symmetry_point(camera.from_grid(mouse.origin),
                                         split_shape_points[0][i].symmetry_key),
                  1, 0);
      }
    }
  }

  function draw(origin, name, colour, radius, symmetry)
  {
    var is_closest = false;
    for (var k = 0; k < closest_objects.length; ++k) {
      if (closest_objects[k].object == origin) {
        is_closest = true;
        break;
      }
    }
    var is_hover = is_closest && !mouse.drag_objects &&
                   !add_shape_points && !split_shape_points &&
                   !add_barrier_points && !symmetry;
    draw_object(change_drag(origin),
                name, colour, is_hover ? 2 : 1, radius, symmetry);
  }

  // Draw objects.
  for (var i = 0; i < level.sling_points.length; ++i) {
    draw(level.sling_points[i], 'sling',
         symmetry ? '#060' : '#0c0', 400, symmetry);
  }
  for (var i = 0; i < level.ball_barriers.length; ++i) {
    draw(level.ball_barriers[i][0], 'barrier',
         symmetry ? '#060' : '#0c0', 8, symmetry);
    draw(level.ball_barriers[i][1], 'barrier',
         symmetry ? '#060' : '#0c0', 8, symmetry);
    context.strokeStyle = symmetry ? '#060' : '#0c0';
    draw_line(change_drag(level.ball_barriers[i][0]),
              change_drag(level.ball_barriers[i][1]),
              1, symmetry);
  }
  if (!symmetry) {
    draw(level.ball_spawn, 'ball', '#ccc', 200, 0);
    draw(level.red_spawn, 'spawn', '#c00', 0, 0);
    draw(level.red_goal, 'goal', '#c00', 16, 0);
    draw(level.blue_spawn, 'spawn', '#00c', 0, 0);
    draw(level.blue_goal, 'goal', '#00c', 16, 0);
  }
}

function draw()
{
  context.clearRect(0, 0, camera.size.x, camera.size.y);
  context.font = '12px monospace';

  fill_level(level);

  // Draw grid.
  var min = camera.min();
  var max = camera.max();
  for (var x = min.x - mod(min.x, camera.grid_size);
       x <= max.x; x += camera.grid_size) {
    context.strokeStyle = x == 0 ? '#333' : '#222';
    draw_line(vec2(x, -min.y), vec2(x, -max.y), 1, 0);
  }
  for (var y = min.y - mod(min.y, camera.grid_size);
       y <= max.y; y += camera.grid_size) {
    context.strokeStyle = y == 0 ? '#333' : '#222';
    draw_line(vec2(min.x, -y), vec2(max.x, -y), 1, 0);
  }

  // Draw cursor.
  context.strokeStyle = '#666';
  var snap = camera.from_grid(mouse.origin);
  var csize = 4 / camera.zoom;
  draw_line(snap.add(vec2(-csize, -csize)), snap.add(vec2(csize, csize)), 1, 0);
  draw_line(snap.add(vec2(csize, -csize)), snap.add(vec2(-csize, csize)), 1, 0);

  var symmetries = editing.get_symmetries();
  for (var i = 0; i < symmetries.length; ++i) {
    draw_level(level, symmetries[i]);
  }

  // Draw help text.
  context.fillStyle = '#ccc';
  context.textAlign = 'left';
  context.textBaseline = 'top';
  var symmetry_text = editing.helper == 1 ? 'horizontal' :
                      editing.helper == 2 ? 'vertical' :
                      editing.helper == 3 ? 'rotational' :
                      editing.helper == 4 ? '4-way mirror' :
                      editing.helper == 5 ? '4-way rotational' :
                                      'off';
  var point_text = snap.x + ', ' + snap.y + ', ' + camera.zoom + 'X';
  if (add_shape_points) {
    draw_text('Click: place point\nReturn: finish', vec2(6, 6));
  }
  else if (split_shape_points) {
    draw_text('Click two points', vec2(6, 6));
  }
  else if (add_barrier_points) {
    draw_text('Click two points', vec2(6, 6));
  }
  else {
    draw_text('Shift+L: load map\nShift+S: save map\nShift+C: clear map\n\n' +
              'G: double grid\nH: halve grid\n' +
              'Shift+G: increase grid\nShift+H: decrease grid\n' +
              '[Grid size: ' + camera.grid_size + ']\n\n' +
              'Y: symmetry helper [' + symmetry_text + ']\n' +
              'U: symmetric editing [' +
                  (editing.symmetric ? 'on' : 'off' ) + ']\n\n' +
              'N: new shape\nS: split edge\nT: split shape\n' +
              'A: add sling point\nShift+B: add ball barrier\nD: delete object\n' +
              'M: merge level\nShift+M: triangulate level\n\n' +
              'C: circular curve\nV: elliptical curve\nB: bezier curve\n\n' +
              'Z: undo\nShift+Z: redo\n\n' +
              'O: zoom out\nP: zoom in\n' +
              'Ctrl+drag: move view\nDrag: move objects\n' +
              '[' + point_text + ']',
              vec2(6, 6));
  }
}

function update_draw()
{
  update();
  draw();
}

function update_closest()
{
  editing.update(camera.from(mouse.origin), level);
}

function do_add_shape()
{
  if (add_shape_points.length < 3) {
    add_shape_points = null;
    return false;
  }

  var actions = array();
  var symmetries = editing.symmetric ? editing.get_symmetries() : [0];
  for (var i = 0; i < symmetries.length; ++i) {
    var list = array();
    for (var j = 0; j < add_shape_points.length; ++j) {
      list.push(editing.symmetry_point(add_shape_points[j], symmetries[i]));
    }
    actions.push(new add_action(level.shapes, level.shapes.length, list));
  }
  undo.add(new compound_action(actions));
  orient_shapes(level);
  add_shape_points = null;
  return true;
}

function do_split_shape()
{
  if (split_shape_points.length != 2) {
    split_shape_points = null;
    return false;
  }

  var arr = array();

  function handle(a, b)
  {
    var ai = -1;
    var bi = -1;
    var aj = 0;
    var bj = 0;
    for (var i = 0; i < level.shapes.length; ++i) {
      for (var j = 0; j < level.shapes[i].length; ++j) {
        if (a == level.shapes[i][j]) {
          ai = i;
          aj = j;
        }
        if (b == level.shapes[i][j]) {
          bi = i;
          bj = j;
        }
      }
    }
    if (ai < 0 || bi < 0 || ai != bi) {
      return;
    }
    var index = ai;
    var min = Math.min(aj, bj);
    var max = Math.max(aj, bj);
    if (max == min + 1 || (min == 0 && max == level.shapes[index].length - 1)) {
      return;
    }
    var new1 = level.shapes[index].slice(min + 1, max);
    new1.push(level.shapes[index][max].copy());
    new1.unshift(level.shapes[index][min].copy());
    var new2 = level.shapes[index].slice(0, min);
    new2.push(level.shapes[index][min].copy());
    new2.push(level.shapes[index][max].copy());
    new2 = new2.concat(level.shapes[index].slice(max + 1));
    var a0 = new delete_action(level.shapes, index);
    a0.redo();
    var a1 = new add_action(level.shapes, level.shapes.length, new1);
    a1.redo();
    var a2 = new add_action(level.shapes, level.shapes.length, new2);
    a2.redo();
    arr.push(a0);
    arr.push(a1);
    arr.push(a2);
  }

  for (var i = 0; i < split_shape_points[0].length; ++i) {
    for (var j = 0; j < split_shape_points[1].length; ++j) {
      if (split_shape_points[0][i].symmetry_key !=
          split_shape_points[1][j].symmetry_key) {
        continue;
      }
      handle(split_shape_points[0][i].object,
             split_shape_points[1][j].object);
    }
  }

  split_shape_points = null;
  if (arr.length) {
    undo.add_no_do(new compound_action(arr));
    orient_shapes(level);
    return true;
  }
  return false;
}

function do_add_barrier()
{
  if (add_barrier_points.length < 2) {
    add_barrier_points = null;
    return false;
  }

  var actions = array();
  var symmetries = editing.symmetric ? editing.get_symmetries() : [0];
  for (var i = 0; i < symmetries.length; ++i) {
    var b = array();
    b.push(editing.symmetry_point(add_barrier_points[0], symmetries[i]));
    b.push(editing.symmetry_point(add_barrier_points[1], symmetries[i]));
    actions.push(new add_action(
        level.ball_barriers, level.ball_barriers.length, b));
  }
  undo.add(new compound_action(actions));
  add_barrier_points = null;
  return true;
}

function do_move()
{
  var m = camera.from_grid(mouse.origin);
  var actions = array();
  for (var i = 0; i < mouse.drag_objects.length; ++i) {
    var to = editing.symmetry_point(m, mouse.drag_objects[i].symmetry_key);
    if (!to.eq(mouse.drag_objects[i].object)) {
      actions.push(new move_action(mouse.drag_objects[i].object, to));
    }
  }
  if (actions.length) {
    undo.add(new compound_action(actions));
    return true;
  }
  return false;
}

function do_split_edge()
{
  var closest = editing.closest_edges.slice(0);
  var actions = array();
  for (var i = 0; i < closest.length; ++i) {
    var a = level.vertex(closest[i].i, closest[i].j);
    var b = level.vertex(closest[i].i, 1 + closest[i].j);
    var a = new add_action(
        level.shapes[closest[i].i], 1 + closest[i].j,
        a.add(b).div(2));
    a.redo();
    actions.push(a);
    for (var j = i + 1; j < closest.length; ++j) {
      if (closest[i].i == closest[j].i &&
          closest[j].j > closest[i].j) {
        ++closest[j].j;
      }
    }
  }
  if (actions.length) {
    undo.add_no_do(new compound_action(actions));
    return true;
  }
  return false;
}

function do_delete()
{
  var closest = editing.closest_objects;
  var actions = array();
  var deleted = array();
  for (var i = 0; i < closest.length; ++i) {
    deleted.push(false);
  }

  for (var i = 0; i < level.sling_points.length; ++i) {
    for (var j = 0; j < closest.length; ++j) {
      if (level.sling_points[i] == closest[j].object && !deleted[j]) {
        var a = new delete_action(level.sling_points, i);
        a.redo();
        actions.push(a);
        deleted[j] = true;
        --i;
        break;
      }
    }
  }
  for (var i = 0; i < level.ball_barriers.length; ++i) {
    for (var j = 0; j < closest.length; ++j) {
      if ((level.ball_barriers[i][0] == closest[j].object ||
           level.ball_barriers[i][1] == closest[j].object) && !deleted[j]) {
        var a = new delete_action(level.ball_barriers, i);
        a.redo();
        actions.push(a);
        deleted[j] = true;
        --i;
        break;
      }
    }
  }
  for (var i = 0; i < level.shapes.length; ++i) {
    for (var j = 0; j < level.shapes[i].length; ++j) {
      for (var k = 0; k < closest.length; ++k) {
        if (level.shapes[i][j] == closest[k].object && !deleted[k]) {
          var a = null;
          if (level.shapes[i].length <= 3) {
            a = new delete_action(level.shapes, i);
            --i;
            j = i >= 0 ? level.shapes[i].length : 0;
          }
          else {
            a = new delete_action(level.shapes[i], j);
            --j;
          }
          a.redo();
          actions.push(a);
          deleted[k] = true;
          break;
        }
      }
      if (i < 0) {
        break;
      }
    }
  }
  if (actions.length) {
    undo.add_no_do(new compound_action(actions));
    return true;
  }
  return false;
}

function do_curve(type)
{
  var closest = editing.closest_objects;
  var actions = array();
  for (var i = 0; i < level.shapes.length; ++i) {
    for (var j = 0; j < level.shapes[i].length; ++j) {
      for (var k = 0; k < closest.length; ++k) {
        if (level.shapes[i][j] == closest[k].object &&
            level.shapes[i].length >= 3) {
          var a = level.vertex(i, j - 1);
          var b = level.vertex(i, j);
          var c = level.vertex(i, 1 + j);
          var curve =
              type == 0 ? get_circular_curve(a, b, c, camera.grid_size) :
              type == 1 ? get_elliptical_curve(a, b, c, camera.grid_size) :
                          get_bezier_curve(a, b, c, camera.grid_size);
          var action = new splice_action(level.shapes[i], j, curve);
          action.redo();
          actions.push(action);
        }
      }
    }
  }
  if (actions.length) {
    undo.add_no_do(new compound_action(actions));
    return true;
  }
  return false;
}

body.onmousemove = function(event)
{
  mouse.origin = vec2(event.clientX, event.clientY);
  update_closest();
  update_draw();
}

canvas.onmousedown = function(event)
{
  var nothing_shown = !shown(load_textarea) && !shown(save_textarea);
  mouse.button = event.ctrlKey || event.shiftKey || event.button == 2 ? 2 : 1;
  mouse.drag_start = mouse.origin;

  if (!add_shape_points && !split_shape_points && !add_barrier_points &&
      nothing_shown && mouse.button == 1) {
    mouse.drag_objects = editing.closest_objects;
    if (!mouse.drag_objects.length) {
      mouse.drag_objects = null;
    }
  }
  update_draw();
  return false;
}

canvas.onmouseup = function(event)
{
  var nothing_shown = !shown(load_textarea) && !shown(save_textarea);
  if (add_shape_points && mouse.button == 1 && nothing_shown) {
    add_shape_points.push(camera.from_grid(mouse.origin));
    update_draw();
    return false;
  }
  if (split_shape_points && mouse.button == 1 && nothing_shown) {
    if (editing.closest_objects && editing.closest_objects.length) {
      split_shape_points.push(editing.closest_objects.slice(0));
      if (split_shape_points.length == 2) {
        if (do_split_shape()) {
          update_closest();
        }
      }
      update_draw();
    }
    return false;
  }
  if (add_barrier_points && mouse.button == 1 && nothing_shown) {
    add_barrier_points.push(camera.from_grid(mouse.origin));
    if (add_barrier_points.length == 2) {
      if (do_add_barrier()) {
        update_closest();
      }
    }
    update_draw();
    return false;
  }
  if (mouse.drag_objects && nothing_shown) {
    if (do_move()) {
      orient_shapes(level);
      update_closest();
    }
  }
  mouse.button = 0;
  mouse.drag_objects = null;
  update_draw();
  return false;
}

body.onkeypress = function(event)
{
  var ch = String.fromCharCode(event.keyCode).toLowerCase();
  var nothing_shown = !shown(load_textarea) && !shown(save_textarea);

  if (add_shape_points) {
    if (event.keyCode == 13) {
      if (do_add_shape()) {
        update_closest();
      }
      update_draw();
      return false;
    }
    return true;
  }
  if (split_shape_points) {
    if (event.keyCode == 13) {
      split_shape_points = null;
      update_closest();
      update_draw();
      return false;
    }
    return true;
  }
  if (mouse.drag_objects && mouse.drag_objects.length) {
    return true;
  }

  // Show load box.
  if (ch == 'l' && event.shiftKey && nothing_shown) {
    show(load_textarea, true);
    return false;
  }
  // Save level.
  if (ch == 's' && event.shiftKey && nothing_shown) {
    save_textarea.value = save_obj(level);
    show(save_textarea, true);
    return false;
  }
  // Load level.
  if (event.keyCode == 13 && shown(load_textarea)) {
    show(load_textarea, false);
    var scale = vec2(1, 1);
    var lev = load_obj(load_textarea.value, scale);
    undo.add(new change_level_action(camera, lev, scale.x));
    orient_shapes(level);
    load_textarea.value = '';
    update_closest();
    update_draw();
    return false;
  }
  // Hide save box.
  if (event.keyCode == 13 && shown(save_textarea)) {
    show(save_textarea, false);
    return false;
  }
  if (!nothing_shown) {
    return true;
  }
  // Clear level.
  if (ch == 'c' && event.shiftKey) {
    undo.add(new change_level_action(camera, level_data(), 16));
    update_closest();
    update_draw();
    return false;
  }
  // Merge level.
  if (ch == 'm') {
    merge = event.shiftKey ? triangulate_to_level(level) : merge_level(level);
    orient_shapes(merge);
    if (merge) {
      undo.add(new change_level_action(camera, merge, 0));
    }
    update_closest();
    update_draw();
    return false;
  }
  // Change grid size.
  if (ch == 'g' && camera.grid_size < 256) {
    camera.grid_size = event.shiftKey ?
        1 + camera.grid_size : camera.grid_size * 2;
    camera.grid_size = camera.grid_size >= 256 ? 256 : camera.grid_size;
    update_closest();
    update_draw();
    return false;
  }
  if (ch == 'h' && camera.grid_size > 2) {
    camera.grid_size = event.shiftKey ?
        camera.grid_size - 1 : camera.grid_size / 2;
    camera.grid_size = camera.grid_size <= 2 ? 2 : camera.grid_size;
    update_closest();
    update_draw();
    return false;
  }
  // Change symmetry helper.
  if (ch == 'y') {
    editing.helper = ((event.shiftKey ? 5 : 1) + editing.helper) % 6;
    update_closest();
    update_draw();
    return false;
  }
  if (ch == 'u') {
    editing.symmetric = !editing.symmetric;
    update_closest();
    update_draw();
    return false;
  }
  // New shape.
  if (ch == 'n') {
    add_shape_points = array();
    update_draw();
    return false;
  }
  // Split edge.
  if (ch == 's' && !event.shiftKey) {
    if (do_split_edge()) {
      orient_shapes(level);
      update_closest();
      update_draw();
    }
    return false;
  }
  // Split shape.
  if (ch == 't') {
    split_shape_points = array();
    update_closest();
    update_draw();
    return false;
  }
  // Add sling point.
  if (ch == 'a') {
    var symmetries = editing.get_symmetries();
    var actions = array();
    for (var i = editing.symmetric ? 0 : symmetries.length - 1;
         i < symmetries.length; ++i) {
      actions.push(new add_action(
          level.sling_points, level.sling_points.length,
          editing.symmetry_point(camera.from_grid(mouse.origin),
                                 symmetries[i])));
    }
    undo.add(new compound_action(actions));
    update_closest();
    update_draw();
    return false;
  }
  // Add barrier point.
  if (ch == 'b' && event.shiftKey) {
    add_barrier_points = array();
    update_draw();
    return false;
  }
  // Delete sling point or shape point.
  if (ch == 'd') {
    if (do_delete()) {
      orient_shapes(level);
      update_closest();
      update_draw();
    }
    return false;
  }
  // Curve creation.
  if (ch == 'c') {
    if (do_curve(0)) {
      orient_shapes(level);
      update_closest();
      update_draw();
    }
    return false;
  }
  if (ch == 'v') {
    if (do_curve(1)) {
      orient_shapes(level);
      update_closest();
      update_draw();
    }
    return false;
  }
  if (ch == 'b' && !event.shiftKey) {
    if (do_curve(2)) {
      orient_shapes(level);
      update_closest();
      update_draw();
    }
    return false;
  }
  // Zoom in/out.
  if (ch == 'o' && camera.zoom > 1 / 256) {
    camera.zoom /= 2;
    update_closest();
    update_draw();
    return false;
  }
  if (ch == 'p' && camera.zoom < 256) {
    camera.zoom *= 2;
    update_closest();
    update_draw();
    return false;
  }
  // Undo/redo.
  if (ch == 'z') {
    if (event.shiftKey) {
      undo.redo();
    }
    else {
      undo.undo();
    }
    orient_shapes(level);
    update_closest();
    update_draw();
    return false;
  }
  return true;
}

load_textarea.onfocus = function()
{
  load_textarea.value = '';
}

update_draw();
